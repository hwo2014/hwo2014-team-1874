using System;
using System.Net.Sockets;
using System.IO;

namespace HelloWorldOpen
{
	public static class Program
	{
		private static Bot[] bots = new Bot[]{ new ConstantSpeedBot(0.655) };
		private static Bot current = bots[0];

		public static void Main (string[] args)
		{
			string host = args[0];
			int port = int.Parse(args[1]);
			string botName = args[2];
			string botKey = args[3];

			Console.WriteLine("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

			using (TcpClient client = new TcpClient(host, port)) {
				NetworkStream stream = client.GetStream();
				StreamReader reader = new StreamReader(stream);
				StreamWriter writer = new StreamWriter(stream);
				writer.AutoFlush = true;

				current.Initialize(botName, botKey, reader, writer);
				current.Run();
			}
		}
	}
}

