using System;
using System.IO;
using Newtonsoft.Json;
using System.Diagnostics;
using System.Threading;

namespace HelloWorldOpen
{
	public class ConstantSpeedBot : Bot
	{
		private class MsgWrapper
		{
			public string msgType;
			public object data;

			public MsgWrapper ()
			{
			}
		}

		private double _speed;

		public ConstantSpeedBot (double speed)
		{
			_speed = speed;
		}

		public override void Run ()
		{
			string line;

			Join(_name, _key, "red");

			int crashes = 0;

			while ((line = _reader.ReadLine()) != null) {
				MsgWrapper msg = JsonConvert.DeserializeObject<MsgWrapper>(line);
				switch (msg.msgType) {
					case "carPositions":
						Throttle(_speed);
						Ping();
						Console.WriteLine("Throttle " +line);
						break;
					case "join":
						Console.WriteLine("Joined");
						break;
					case "gameInit":
						Console.WriteLine("Race init");
						break;
					case "gameEnd":
						Console.WriteLine("Race ended");
						break;
					case "gameStart":
						Console.WriteLine("Race starts");
						break;
					case "yourCar":
						Console.WriteLine ("yourCar");
						break;
					case "lapFinished":
						Console.WriteLine ("lapFinished");
						break;
					case "crash":
						Console.WriteLine("crash");
						crashes++;
						Ping ();
						break;
					case "spawn":
						Console.WriteLine ("spawn");
						break;
					case "finish":
						Console.WriteLine("finish");
						break;
					case "tournamentEnd":
						Console.WriteLine ("tournamentEnd");
						break;
					default:
						Console.WriteLine("unknown ");
						Console.WriteLine(line);
						break;
				}
			}

			Console.WriteLine("Disconnect");

			Console.WriteLine ("#Crashes: "+crashes);
		}
	}
}

