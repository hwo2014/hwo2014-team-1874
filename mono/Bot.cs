using System;
using System.IO;
using System.Net.Sockets;
using Newtonsoft.Json;
using System.Threading;

public abstract class Bot
{
	protected string _name;
	protected string _key;
	protected StreamReader _reader;
	protected StreamWriter _writer;

	public Bot ()
	{
	}

	public void Initialize (string name, string key, StreamReader reader, StreamWriter writer)
	{
		_name = name;
		_key = key;
		_reader = reader;
		_writer = writer;
	}

	public abstract void Run();

	protected void Send(string msgType, object data) 
	{
		string msg = JsonConvert.SerializeObject(new {msgType, data });
		_writer.WriteLine (msg);
	}

	protected void Ping ()
	{
		Send ("ping", new {});
	}

	protected void Throttle (double value)
	{
		Send ("throttle", value);
	}

	protected void Join (string name, string key, string color)
	{
		Send ("join", new {name, key, color});
	}
}
